package ru.t1.sochilenkov.tm;

import ru.t1.sochilenkov.tm.constant.ArgumentConstant;

public class Application {

    public static void main(String[] args) {
        showWelcome();
        processArguments(args);
        System.exit(0);
    }

    public static void processArguments (final String[] arguments){
        if (arguments == null || arguments.length < 1) {
            showError();
            return;
        }
        processArgument(arguments[0]);
    }

    public static void processArgument(final String argument){
        switch(argument){
            case ArgumentConstant.VERSION :
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            default: showError();
        }
    }

    public static void showWelcome(){
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public  static void showError(){
        System.out.println("[ERROR]");
        System.out.println("Input program arguments are not correct");
    }

    public static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikita Sochilenkov");
        System.out.println("E-mail: nsochilenkov@t1-consulting.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show version info\n", ArgumentConstant.VERSION);
        System.out.printf("%s - Show developer info\n", ArgumentConstant.ABOUT);
        System.out.printf("%s - Show help info\n", ArgumentConstant.HELP);
    }

}